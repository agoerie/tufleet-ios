//
//  OrderModel.swift
//  Tufleet
//
//  Created by Agoerie on 10/15/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import Foundation
import SwiftyJSON

class OrderModel {
    var idBooking: String
    var tipe: String
    var trip: String
    var tanggal: String
    var jam: String
    var driver: String
    var noHp: String
    var mobil: String
    var from: String
    var attachment: String
    var status: String
    var remark: String
    var rating: String
    var review: String
    var remarkCancel: String
    var destination: [DestinationModel]
    
    init(idBooking: String,tipe: String,trip: String,tanggal: String,jam: String,driver: String,noHp: String,mobil: String,from: String,attachment: String,status: String,remark: String,rating: String,review: String,remarkCancel: String,destination: [DestinationModel]) {
        self.idBooking = idBooking
        self.tipe = tipe
        self.trip = trip
        self.tanggal = tanggal
        self.jam = jam
        self.driver = driver
        self.noHp = noHp
        self.mobil = mobil
        self.from = from
        self.attachment = attachment
        self.status = status
        self.remark = remark
        self.rating = rating
        self.review = review
        self.remarkCancel = remarkCancel
        self.destination = destination
    }
    
    convenience init(json: JSON){
        let idBooking = json["id_booking"].stringValue
        let tipe = json["tipe"].stringValue
        let trip = json["trip"].stringValue
        let tanggal = json["tanggal"].stringValue
        let jam = json["jam"].stringValue
        let driver = json["driver"].stringValue
        let noHp = json["no_hp"].stringValue
        let mobil = json["mobil"].stringValue
        let from = json["from"].stringValue
        let attachment = json["attachment"].stringValue
        let status = json["status"].stringValue
        let remark = json["remark"].stringValue
        let rating = json["rating"].stringValue
        let review = json["review"].stringValue
        let remarkCancel = json["remarkCancel"].stringValue
        
        var destinations = [DestinationModel]()
        for value in json["destinasi"].arrayValue {
            let destination = DestinationModel(json: value)
            destinations.append(destination)
        }
        
        self.init(idBooking: idBooking, tipe: tipe, trip: trip, tanggal: tanggal, jam: jam, driver: driver, noHp: noHp, mobil: mobil, from: from, attachment: attachment, status: status, remark: remark, rating: rating, review: review, remarkCancel: remarkCancel, destination: destinations)
    }
    
}
