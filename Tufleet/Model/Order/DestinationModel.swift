//
//  DestinationModel.swift
//  Tufleet
//
//  Created by Agoerie on 10/15/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import Foundation
import SwiftyJSON


class DestinationModel {
    var nama: String
    
    init(nama: String) {
        self.nama = nama
    }
    
    convenience init(json: JSON){
        let nama = json["nama"].stringValue
        
        self.init(nama: nama)
    }
    
}
