//
//  ListGroup.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/3/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON


class ListGroup {
    var id: Int
    var groupName: String
    
    init(id: Int,groupName: String) {
        self.id = id
        self.groupName = groupName
    }
    
    convenience init(json: JSON){
        let id = json["id"].intValue
        let groupName = json["group_name"].stringValue
        
        self.init(id: id, groupName: groupName)
    }
    
}
