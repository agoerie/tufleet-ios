//
//  OptionContent.swift
//  Tufleet
//
//  Created by Agoerie on 9/26/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import MaterialDesignSymbol

class OptionContent: UIView {
    
    @IBOutlet weak var tables: UITableView!{
        didSet{
            let xib = OptionCell.nib
            tables.register(xib, forCellReuseIdentifier: OptionCell.identifier)
            
            tables.separatorStyle = .none
            tables.tableFooterView = UIView()
            tables.backgroundColor = UIColor.clear
            tables.isScrollEnabled = false
            
            tables.dataSource = self
            tables.delegate = self
        }
    }
    
    @IBOutlet var tableViewContraint: NSLayoutConstraint!
    
    var data: [String] = []
    var selectData: String = ""

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = 5
    }

}

extension OptionContent: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OptionCell.identifier, for: indexPath) as! OptionCell
        
        cell.optionLbl.text = data[indexPath.row]
        
        if indexPath.row == (data.count - 1) {
            cell.separator.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OptionCell
        cell.icoLbl.text = MaterialDesignIcon.radioButtonOn24px
        
        selectData = cell.optionLbl.text!
        
        NotificationCenter.default.post(name: .transporType, object: cell, userInfo: nil)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OptionCell
        cell.icoLbl.text = MaterialDesignIcon.radioButtonOff24px
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableViewContraint.constant = tables.contentSize.height
    }
}

extension OptionContent: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
