//
//  SummaryDetailContent.swift
//  Tufleet
//
//  Created by Agoerie on 10/16/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import MaterialDesignSymbol

class SummaryDetailContent: UIView {
    
    var titleOrderLbl = UILabel()
    var valOrderLbl = UILabel()
    var statusLbl = UILabel()
    var icoLbl = UILabel()
    var titleTypeLbl = UILabel()
    var valTypeLbl = UILabel()
    var titleFromLbl = UILabel()
    var valFromLbl = UILabel()
    var titleDestLbl = UILabel()
    var valDestView = SummaryDestination()
    var titleDateLbl = UILabel()
    var valDateLbl = UILabel()
    var titleHourLbl = UILabel()
    var valHourLbl = UILabel()
    var titleDriverLbl = UILabel()
    var valDriverLbl = UILabel()
    var titleContactLbl = UILabel()
    var valContactLbl = UILabel()
    var titleCarLbl = UILabel()
    var valCarLbl = UILabel()
    var titleRemarkLbl = UILabel()
    var valRemarkLbl = UILabel()
    
    var data: OrderModel?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        titleOrderLbl.text = "Order : "
        titleOrderLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleOrderLbl.textColor = UIColor.darkGray
        self.addSubview(titleOrderLbl)
        
        valOrderLbl.text = data?.trip
        valOrderLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valOrderLbl.textColor = UIColor.darkGray
        self.addSubview(valOrderLbl)
        
        statusLbl.text = data?.status
        statusLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        statusLbl.textColor = UIColor.darkGray
        self.addSubview(statusLbl)
        
        icoLbl.font = MaterialDesignFont.fontOfSize(18)
        icoLbl.text = MaterialDesignIcon.radioButtonOn24px
        icoLbl.textColor = UIColor.init(hexString: "#797979")
        self.addSubview(icoLbl)
        
        titleTypeLbl.text = "Type of Trip : "
        titleTypeLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleTypeLbl.textColor = UIColor.darkGray
        self.addSubview(titleTypeLbl)
        
        valTypeLbl.text = data?.tipe
        valTypeLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valTypeLbl.textColor = UIColor.darkGray
        self.addSubview(valTypeLbl)
        
        titleFromLbl.text = "From : "
        titleFromLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleFromLbl.textColor = UIColor.darkGray
        self.addSubview(titleFromLbl)
        
        valFromLbl.text = data?.from
        valFromLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valFromLbl.numberOfLines = 0
        valFromLbl.textColor = UIColor.darkGray
        self.addSubview(valFromLbl)
        
        titleDestLbl.text = "Destination : "
        titleDestLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleDestLbl.textColor = UIColor.darkGray
        self.addSubview(titleDestLbl)
        
        valDestView = Bundle.main.loadNibNamed("SummaryDestination", owner: nil, options: nil)?.first as! SummaryDestination
        valDestView.data = data?.destination
        self.addSubview(valDestView)
        
        titleDateLbl.text = "Date : "
        titleDateLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleDateLbl.textColor = UIColor.darkGray
        self.addSubview(titleDateLbl)
        
        valDateLbl.text = data?.tanggal
        valDateLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valDateLbl.textColor = UIColor.darkGray
        self.addSubview(valDateLbl)
        
        titleHourLbl.text = "Hour : "
        titleHourLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleHourLbl.textColor = UIColor.darkGray
        self.addSubview(titleHourLbl)
        
        valHourLbl.text = data?.jam
        valHourLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valHourLbl.textColor = UIColor.darkGray
        self.addSubview(valHourLbl)
        
        titleDriverLbl.text = "Driver : "
        titleDriverLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleDriverLbl.textColor = UIColor.darkGray
        self.addSubview(titleDriverLbl)
        
        valDriverLbl.text = data?.driver
        valDriverLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valDriverLbl.textColor = UIColor.darkGray
        self.addSubview(valDriverLbl)
        
        titleContactLbl.text = "Driver Contact : "
        titleContactLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleContactLbl.textColor = UIColor.darkGray
        self.addSubview(titleContactLbl)
        
        valContactLbl.text = data?.noHp
        valContactLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valContactLbl.textColor = UIColor.darkGray
        self.addSubview(valContactLbl)
        
        titleCarLbl.text = "Mobil : "
        titleCarLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleCarLbl.textColor = UIColor.darkGray
        self.addSubview(titleCarLbl)
        
        valCarLbl.text = data?.mobil
        valCarLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valCarLbl.textColor = UIColor.darkGray
        self.addSubview(valCarLbl)
        
        titleRemarkLbl.text = "Remark : "
        titleRemarkLbl.font = UIFont.init(name: "AvenirNext-Regular", size: 14)
        titleRemarkLbl.textColor = UIColor.darkGray
        self.addSubview(titleRemarkLbl)
        
        valRemarkLbl.text = data?.remark
        valRemarkLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 14)
        valRemarkLbl.numberOfLines = 0
        valRemarkLbl.textColor = UIColor.darkGray
        self.addSubview(valRemarkLbl)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        titleOrderLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valOrderLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(15)
            make.leading.equalTo(titleOrderLbl.snp.trailing).offset(0)
        }
        
        statusLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(15)
            make.trailing.equalTo(icoLbl.snp.leading).inset(-10)
        }
        
        icoLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(15)
            make.trailing.equalTo(self.snp.trailing).inset(20)
        }
        
        titleTypeLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valOrderLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valTypeLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valOrderLbl.snp.bottom).offset(15)
            make.leading.equalTo(titleTypeLbl.snp.trailing).offset(0)
        }
        
        titleFromLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valTypeLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valFromLbl.snp.makeConstraints { (make) in
            make.top.equalTo(titleFromLbl.snp.bottom).offset(5)
            make.leading.equalTo(self.snp.leading).offset(20)
            make.trailing.equalTo(self.snp.trailing).inset(20)
        }
        
        titleDestLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valFromLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valDestView.snp.makeConstraints { (make) in
            make.top.equalTo(titleDestLbl.snp.bottom).offset(0)
            make.leading.equalTo(self.snp.leading).offset(20)
            make.trailing.equalTo(self.snp.trailing).inset(20)
        }
        
        titleDateLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valDestView.snp.bottom).offset(20)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valDateLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valDestView.snp.bottom).offset(20)
            make.leading.equalTo(titleDateLbl.snp.trailing).offset(0)
        }
        
        titleHourLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valDateLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valHourLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valDateLbl.snp.bottom).offset(15)
            make.leading.equalTo(titleHourLbl.snp.trailing).offset(0)
        }
        
        titleDriverLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valHourLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valDriverLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valHourLbl.snp.bottom).offset(15)
            make.leading.equalTo(titleDriverLbl.snp.trailing).offset(0)
        }
        
        titleContactLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valDriverLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valContactLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valDriverLbl.snp.bottom).offset(15)
            make.leading.equalTo(titleContactLbl.snp.trailing).offset(0)
        }
        
        titleCarLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valContactLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valCarLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valContactLbl.snp.bottom).offset(15)
            make.leading.equalTo(titleCarLbl.snp.trailing).offset(0)
        }
        
        titleRemarkLbl.snp.makeConstraints { (make) in
            make.top.equalTo(valCarLbl.snp.bottom).offset(15)
            make.leading.equalTo(self.snp.leading).offset(20)
        }
        
        valRemarkLbl.snp.makeConstraints { (make) in
            make.top.equalTo(titleRemarkLbl.snp.bottom).offset(5)
            make.leading.equalTo(self.snp.leading).offset(20)
            make.trailing.equalTo(self.snp.trailing).inset(20)
            make.bottom.equalTo(self.snp.bottom).inset(15)
        }
    }

}
