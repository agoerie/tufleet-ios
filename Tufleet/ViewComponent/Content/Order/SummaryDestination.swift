//
//  SummaryDestination.swift
//  Tufleet
//
//  Created by Agoerie on 10/18/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit

protocol destDelegate: class {
    func onDelete(cell: SummaryDestination, row: Int)
}

class SummaryDestination: UIView {
    
    var table = UITableView()
    
    var data: [DestinationModel]?
    
    open weak var delegate: destDelegate?
    
    var isDisableBtn: Bool = true

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.backgroundColor = UIColor.clear
        
        let xib = SummaryDestinationCell.nib
        table.register(xib, forCellReuseIdentifier: SummaryDestinationCell.identifier)
        table.delegate = self
        table.dataSource = self
        table.isScrollEnabled = false
        table.separatorStyle = .none
        table.tableFooterView = UIView()
        table.backgroundColor = UIColor.clear
        self.addSubview(table)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        table.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leading).offset(0)
            make.trailing.equalTo(self.snp.trailing).offset(0)
            make.top.equalTo(self.snp.top).offset(0)
            make.bottom.equalTo(self.snp.bottom).offset(0)
            make.height.equalTo(200)
        }
    }
    
    func updatesConstraint(margin: CGFloat) {
        table.snp.updateConstraints { (make) in
            make.leading.equalTo(self.snp.leading).offset(margin)
            make.trailing.equalTo(self.snp.trailing).inset(margin)
        }
    }
    
    func updateTableConstraint(margin: CGFloat) {
        table.snp.updateConstraints { (make) in
            make.height.equalTo(0.5)
        }
    }

}

extension SummaryDestination: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data != nil ? data!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SummaryDestinationCell.identifier, for: indexPath) as! SummaryDestinationCell
        
        cell.imageClose.isHidden = isDisableBtn ? true : false
        cell.btnClose.isEnabled = isDisableBtn ? false : true
        
        cell.titleLbl.text = data![indexPath.row].nama
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        table.snp.updateConstraints { (make) in
            make.height.equalTo(table.contentSize.height)
        }
    }
}

extension SummaryDestination: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension SummaryDestination: destCellDelegate {
    func onDeleteCell(cell: SummaryDestinationCell) {
        let indexpath = table.indexPath(for: cell)
        delegate?.onDelete(cell: self, row: (indexpath?.row)!)
    }
}
