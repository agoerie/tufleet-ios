//
//  SummaryListContent.swift
//  Tufleet
//
//  Created by Agoerie on 9/29/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import MaterialDesignSymbol

class SummaryListContent: UIView {
    
    @IBOutlet var driverLbl: UILabel!
    @IBOutlet var phoneLbl: UILabel!
    @IBOutlet var carLbl: UILabel!
    @IBOutlet var statusLbl: UILabel!
    @IBOutlet var icoLbl: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        icoLbl.font = MaterialDesignFont.fontOfSize(18)
        icoLbl.text = MaterialDesignIcon.radioButtonOn24px
        icoLbl.textColor = UIColor.init(hexString: "#797979")
    }

}
