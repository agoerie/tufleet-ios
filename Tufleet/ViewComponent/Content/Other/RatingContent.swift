//
//  RatingContent.swift
//  Tufleet
//
//  Created by Agoerie on 10/27/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import HCSStarRatingView

class RatingContent: UIView {
    
    var stackVw = UIStackView()
    var reviewLbl = UILabel()
    var ratingView = HCSStarRatingView()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        stackVw.axis = .vertical
        stackVw.distribution = .fillEqually
        stackVw.alignment = .fill
        stackVw.spacing = 10
        self.addSubview(stackVw)
        
        reviewLbl.text = "TRAVEL DETAIL"
        reviewLbl.textAlignment = .center
        reviewLbl.textColor = UIColor.init(hexString: "#fc4582")
        reviewLbl.font = UIFont.init(name: "AvenirNext-DemiBold", size: 17)
        stackVw.addArrangedSubview(reviewLbl)
        
        ratingView = HCSStarRatingView.init(frame: CGRect.zero)
        ratingView.backgroundColor = UIColor.clear
        ratingView.maximumValue = 5;
        ratingView.minimumValue = 0;
        ratingView.value = 0;
        ratingView.starBorderColor = UIColor.lightGray
        ratingView.tintColor = UIColor.init(hexString: "#FFFF00")
        ratingView.addTarget(self, action: #selector(changeValue(sender:)), for: .valueChanged)
        stackVw.addArrangedSubview(ratingView)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        stackVw.snp.makeConstraints { (make) in
            make.left.right.equalTo(self).inset(50)
            make.top.bottom.equalTo(self).inset(10)
        }
        
        ratingView.snp.makeConstraints { (make) in
            make.height.equalTo(30)
        }
        
        /*reviewLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(10)
            make.leading.equalTo(self.snp.leading).offset(50)
            make.trailing.equalTo(self.snp.trailing).inset(50)
            make.bottom.equalTo(self.snp.bottom).inset(10)
        }
        
        ratingView.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(10)
            make.leading.equalTo(self.snp.leading).offset(50)
            make.trailing.equalTo(self.snp.trailing).inset(50)
            make.bottom.equalTo(self.snp.bottom).inset(10)
        }*/
    }
    
    @objc func changeValue(sender: HCSStarRatingView) {
        print(sender.value)
    }

}
