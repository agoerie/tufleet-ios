//
//  InputIcoContent.swift
//  Tufleet
//
//  Created by Agoerie on 9/28/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import MaterialDesignSymbol

class InputIcoContent: UIView {
    
    @IBOutlet var inputTF: UITextField!
    @IBOutlet var icoLbl: UILabel!
    
    var isDate: Bool = false
    var dateSelect: String = ""

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        icoLbl.font = MaterialDesignFont.fontOfSize(18)
        icoLbl.text = MaterialDesignIcon.accessTime24px
        icoLbl.textColor = UIColor.init(hexString: "#797979")
        
        inputTF.delegate = self
    }
    
    @objc func datePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        if sender.tag == 1 {
            dateFormatter.dateFormat = "dd-MM-yyyy"
        } else {
            dateFormatter.dateFormat = "HH:mm"
        }
        
        inputTF.text = dateFormatter.string(from: sender.date)
        dateSelect = dateFormatter.string(from: sender.date)
    }

}

extension InputIcoContent: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if isDate {
            if textField.tag == 1 {
                let datePickerView: UIDatePicker = UIDatePicker()
                datePickerView.datePickerMode = UIDatePickerMode.date
                datePickerView.tag = 1
                textField.inputView = datePickerView
                datePickerView.addTarget(self, action: #selector(self.datePickerFromValueChanged), for: UIControlEvents.valueChanged)
            }
            
            if textField.tag == 2 {
                let datePickerView: UIDatePicker = UIDatePicker()
                datePickerView.datePickerMode = UIDatePickerMode.date
                datePickerView.datePickerMode = .time
                datePickerView.tag = 2
                textField.inputView = datePickerView
                datePickerView.addTarget(self, action: #selector(self.datePickerFromValueChanged), for: UIControlEvents.valueChanged)
            }
        }
        
        return true
    }
}
