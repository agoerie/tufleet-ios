//
//  InputContent.swift
//  Tufleet
//
//  Created by Agoerie on 9/27/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit

protocol inputContentDelegate: class {
    func onTFDidUpdate(view: InputContent)
}

class InputContent: UIView {
    
    @IBOutlet weak var tables: UITableView!{
        didSet{
            let xib = InputCell.nib
            tables.register(xib, forCellReuseIdentifier: InputCell.identifier)
            
            tables.separatorStyle = .none
            tables.tableFooterView = UIView()
            tables.backgroundColor = UIColor.clear
            tables.isScrollEnabled = false
            
            tables.dataSource = self
            tables.delegate = self
        }
    }
    
    @IBOutlet var tableViewContraint: NSLayoutConstraint!
    
    var data: [String] = []
    var selectTF: String = ""
    var isLoc: Bool = false
    var tags = 1
    
    open weak var delegate: inputContentDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = 5
    }

}

extension InputContent: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InputCell.identifier, for: indexPath) as! InputCell
        
        cell.inputTF.placeholder = data[indexPath.row]
        cell.inputTF.delegate = self
        cell.inputTF.tag = indexPath.row + 1
        
        if indexPath.row == (data.count - 1) {
            cell.separator.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableViewContraint.constant = tables.contentSize.height
    }
}

extension InputContent: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension InputContent: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if isLoc {
            tags = textField.tag
            
            delegate?.onTFDidUpdate(view: self)
            
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectTF = textField.text!
    }
}
