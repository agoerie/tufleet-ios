//
//  ButtonView.swift
//  Tufleet
//
//  Created by Agoerie on 8/8/18.
//  Copyright © 2018 mdc. All rights reserved.
//

import UIKit

protocol ButtonAppDelegate {
    func submit(view: ButtonView)
}

class ButtonView: UIView {
    
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    var delegate: ButtonAppDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func submit(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }) { (status) in
            self.alpha = 1.0
        }
        
        delegate?.submit(view: self)
    }

}
