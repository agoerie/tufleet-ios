//
//  NavBarLogo.swift
//  Tufleet
//
//  Created by Agoerie on 9/18/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit

protocol NavbarLogoDelegate {
    func menu(navbar: UIView)
}

class NavBarLogo: UIView {
    
    @IBOutlet var logoIV: UIImageView!
    @IBOutlet var nameLbl: UILabel!

    var delegate: NavbarLogoDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        delegate?.menu(navbar: self)
    }


}
