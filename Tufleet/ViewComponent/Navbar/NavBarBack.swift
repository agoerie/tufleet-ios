//
//  NavBarBack.swift
//  Tufleet
//
//  Created by Agoerie on 9/18/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit

protocol NavbarBackDelegate {
    func back(navbar: UIView)
}

class NavBarBack: UIView {

    @IBOutlet var titleLbl: UILabel!
    
    var delegate: NavbarBackDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        delegate?.back(navbar: self)
    }

}
