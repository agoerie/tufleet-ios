//
//  SummaryDestinationCell.swift
//  Tufleet
//
//  Created by Agoerie on 10/18/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit

protocol destCellDelegate: class {
    func onDeleteCell(cell: SummaryDestinationCell)
}

class SummaryDestinationCell: UITableViewCell {
    
    @IBOutlet var imageClose: UIImageView!
    @IBOutlet var btnClose: UIButton!
    
    var titleLbl = UILabel()
    var separator = UIView()

    open weak var delegate: destCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }
    
    func setupView() {
        self.backgroundColor = UIColor.clear
        
        titleLbl.font = UIFont.init(name: "AvenirNext-DemiBoldItalic", size: 14)
        titleLbl.textColor = UIColor.darkGray
        titleLbl.numberOfLines = 0
        self.contentView.addSubview(titleLbl)
        
        separator.backgroundColor = UIColor.lightGray
        self.contentView.addSubview(separator)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView.snp.top).offset(5)
            make.trailing.equalTo(self.contentView.snp.trailing).inset(0)
            make.leading.equalTo(self.contentView.snp.leading).offset(0)
            make.bottom.equalTo(self.contentView.snp.bottom).inset(5)
        }
        
        separator.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.contentView.snp.bottom).offset(0)
            make.trailing.equalTo(self.contentView.snp.trailing).inset(0)
            make.leading.equalTo(self.contentView.snp.leading).offset(0)
            make.height.equalTo(0.5)
        }
    }

    @IBAction func closeAction(_ sender: UIButton) {
        delegate?.onDeleteCell(cell: self)
    }
    
}

extension SummaryDestinationCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SummaryDestinationCell.identifier, for: indexPath) as! SummaryDestinationCell
        
        return cell
    }
}
