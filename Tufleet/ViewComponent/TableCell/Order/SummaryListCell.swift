//
//  SummaryListCell.swift
//  Tufleet
//
//  Created by Agoerie on 9/29/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit

class SummaryListCell: UITableViewCell {
    
    var content = SummaryListContent()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }
    
    func setupView() {
        self.backgroundColor = UIColor.clear
        
        content = Bundle.main.loadNibNamed("SummaryListContent", owner: nil, options: nil)?.first as! SummaryListContent
        content.layer.cornerRadius = 5
        self.contentView.addSubview(content)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        content.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView.snp.top).offset(5)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-5)
        }
    }

}

extension SummaryListCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SummaryListCell.identifier, for: indexPath) as! SummaryListCell
        guard let data = object as? OrderModel else { return cell }
        
        cell.content.driverLbl.text = data.driver == "" ? "-" : data.driver
        cell.content.phoneLbl.text = data.noHp == "" ? "-" : data.noHp
        cell.content.statusLbl.text = data.status
        cell.content.carLbl.text = data.mobil
        
        return cell
    }
}
