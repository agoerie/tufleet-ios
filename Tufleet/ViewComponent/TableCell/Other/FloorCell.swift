//
//  FloorCell.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/3/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit

class FloorCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }

}

extension FloorCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FloorCell.identifier, for: indexPath) as! FloorCell
        
        guard let data = object as? [String] else { return cell }
        
        cell.lblName.text = data[indexPath.row]
        
        return cell
    }
}
