//
//  OptionCell.swift
//  Tufleet
//
//  Created by Agoerie on 9/26/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import MaterialDesignSymbol

class OptionCell: UITableViewCell {
    
    @IBOutlet var icoLbl: UILabel!
    @IBOutlet var optionLbl: UILabel!
    
    var separator = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }
    
    func setupView() {
        icoLbl.font = MaterialDesignFont.fontOfSize(18)
        icoLbl.text = MaterialDesignIcon.radioButtonOff24px
        icoLbl.textColor = UIColor.init(hexString: "#797979")
        
        separator.backgroundColor = UIColor.init(hexString: "#da5788")
        self.addSubview(separator)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        separator.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leading).offset(0)
            make.trailing.equalTo(self.snp.trailing).inset(0)
            make.bottom.equalTo(self.snp.bottom).inset(0)
            make.height.equalTo(1)
        }
    }

}

extension OptionCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OptionCell.identifier, for: indexPath) as! OptionCell
        
        return cell
    }
}
