//
//  AuthController.swift
//  Tufleet
//
//  Created by Agoerie on 10/9/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class UserController: BaseController {
    
    fileprivate let loginAPI = REST.ServiceAPI.User.login.ENV
    fileprivate let forgotPassAPI = REST.ServiceAPI.User.forgot.ENV
    fileprivate let registerAPI = REST.ServiceAPI.User.register.ENV
    fileprivate let editPasswordAPI = REST.ServiceAPI.User.editPassword.ENV
    
    func postLogin(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                   onFailed: @escaping MessageListener,
                   onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: loginAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].stringValue == "success" {
                    let userDt = data["response"][0]
                    
                    self.httpHelper.uid = userDt["uid"].stringValue
                    
                    onSuccess(200, data["message"].stringValue, [userDt["id"].stringValue,userDt["username"].stringValue,userDt["name"].stringValue,userDt["uid"].stringValue,userDt["email"].stringValue,userDt["no_hp"].stringValue,userDt["group_name"].stringValue])
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postForgotPass(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                        onFailed: @escaping MessageListener,
                        onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: forgotPassAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["response"].stringValue == "1" {
                    onSuccess(200, data["message"].stringValue, ["Success"])
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postRegister(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                      onFailed: @escaping MessageListener,
                      onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: registerAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["response"].stringValue == "1" {
                    onSuccess(200, data["message"].stringValue, ["Success"])
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postEditPassword(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                          onFailed: @escaping MessageListener,
                          onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: editPasswordAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["response"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["response"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
}
