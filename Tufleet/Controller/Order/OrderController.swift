//
//  OrderController.swift
//  Tufleet
//
//  Created by Agoerie on 10/15/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import Foundation

import Foundation
import SwiftyJSON
import Alamofire

class OrderController: BaseController {
    
    fileprivate let listOrderAPI = REST.ServiceAPI.Order.listOrder.ENV
    fileprivate let createOrderAPI = REST.ServiceAPI.Order.createOrder.ENV
    fileprivate let reviewOrderAPI = REST.ServiceAPI.Order.reviewOrder.ENV
    fileprivate let checkReviewOrderAPI = REST.ServiceAPI.Order.checkReviewOrder.ENV
    fileprivate let uploadAttachmentAPI = REST.ServiceAPI.Order.uploadAttachmentOrder.ENV
    
    func getListOrder(onSuccess: @escaping CollectionResultListener<OrderModel>,
                      onFailed: @escaping MessageListener,
                      onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: listOrderAPI, param: nil, method: .get) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }

                if data["response"].arrayValue.count > 0 {
                    var orders = [OrderModel]()
                    for value in data["response"].arrayValue {
                        let order = OrderModel(json: value)
                        orders.append(order)
                    }
                    
                    onSuccess(200, "Success fetching data", orders)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postOrder(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                   onFailed: @escaping MessageListener,
                   onComplete: @escaping MessageListener) {
        
        let params: Parameters = param

        httpHelper.requestJSON(url: createOrderAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["response"].intValue == 1 {
                    onSuccess(200, "Success fetching data", [data["message"].stringValue, data["id_booking"].stringValue])
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postReview(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                   onFailed: @escaping MessageListener,
                   onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: reviewOrderAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["response"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func checkReview(onSuccess: @escaping SingleResultListener<String>,
                      onFailed: @escaping MessageListener,
                      onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: checkReviewOrderAPI, param: nil, method: .get) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].stringValue != "error" {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func uploadImage(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                     onFailed: @escaping MessageListener,
                     onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.requestFormData(url: uploadAttachmentAPI, param: params) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].stringValue != "error" {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["status"].stringValue != "" {
                    onFailed(json!["message"].stringValue)
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
}
