//
//  Constants.swift
//  ProjectStructure
//
//  Created by Agung Widjaja on 6/16/17.
//  Copyright © 2017 Agung. All rights reserved.
//

import Foundation

enum BuildType {
    case development
    case stagging
    case production
}

struct Config {
    static let appKey = "a5120ae232de05c4b3b54a1a8a8bfd17"
    
    /*
     *  Define URL of REST API
     */
    
    static let baseAPI: BuildType = .production
    
    static let developmentBaseAPI = "https://tpigaerp.com/api-tufleet-dev"
    static let staggingBaseAPI = "https://tpigaerp.com/api-tufleet-stagging"
    static let productionBaseAPI = "https://tpigaerp.com/api-tufleet"
    
    //"https://www.truvel.com/api/Mobile_API/"
    //"http://staging.truvel.com/api/Mobile_API/"
    
    /*
     *  Define base URL of picture
     */
    static let basePictureProdAPI = "https://tpigaerp.com/tofap/assets/img/"
    static let basePictureDevAPI = "https://tpigaerp.com/tofapdev/assets/img/"
    
    /*
     *  Define base URL of report
     */
    static let reportProdAPI = "https://tpigaerp.com/tofap"
    static let reportDevAPI = "https://tpigaerp.com/tofapdev"
}

struct REST {
    struct ServiceAPI {
        struct Menu {
            static let group = "/group"
        }
        
        struct User {
            static let login = "/emp_auth"
            static let forgot = "/reset_password"
            static let register = "/registrasi"
            static let editPassword = "/emp/change_password"
        }
        
        struct Order {
            static let listOrder = "/emp/order_list"
            static let createOrder = "/emp/new_order"
            static let reviewOrder = "/emp/submit_review"
            static let checkReviewOrder = "/emp/is_reviewed"
            static let uploadAttachmentOrder = "/emp/upload_attachment"
        }
    }
    
    struct StoryboardReferences {
        static let main = "Main"
        static let order = "Order"
        static let user = "User"
        
    }
    
    struct ViewControllerID {
        struct Menu {
            static let bar = "MenuBarViewController"
            static let floor = "FloorViewController"
            static let group = "GroupViewController"
        }
        
        struct User {
            static let login = "LoginController"
            static let forgot = "ForgotPasswordController"
            static let register = "RegisterViewController"
            static let editProfile = "EditProfileViewController"
            static let editPassword = "EditPasswordViewController"
            static let report = "ReportViewController"
        }
        
        struct Order {
            static let list = "ListOrderViewController"
            static let detail = "DetailOrderViewController"
            static let listItem = "ListItemViewController"
            static let orderSummary = "OrderSummaryViewController"
            static let successOrder = "SuccessOrderViewController"
            static let rating = "RatingViewController"
        }
    }
}
