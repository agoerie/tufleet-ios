//
//  ViewController.swift
//  Tufleet
//
//  Created by Agoerie on 9/17/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import AnimatedTextInput

class ViewController: UIViewController {
    
    var navbar = NavBarBack()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setupView() {
        self.view.backgroundColor = UIColor.white
        
        setupConstraint()
    }
    
    func setupConstraint() {
        
    }

}

extension ViewController: NavbarBackDelegate {
    func back(navbar: UIView) {
        print("Masuk Pak Eko")
    }
}

