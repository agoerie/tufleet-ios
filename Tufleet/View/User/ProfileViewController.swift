//
//  ProfileViewController.swift
//  Tufleet
//
//  Created by Agoerie on 10/26/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import AnimatedTextInput

class ProfileViewController: UIViewController {
    
    var btnClose = ButtonView()
    var bgImage = UIImageView()
    var logoImage = UIImageView()
    var passwordOldTF = AnimatedTextInput()
    var passwordNewTF = AnimatedTextInput()
    var btn = ButtonView()
    var activityIndicator: UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        bgImage.image = UIImage.init(named: "bgtugu.jpg")
        bgImage.clipsToBounds = true
        bgImage.contentMode = .scaleToFill
        self.view.addSubview(bgImage)
        
        btnClose = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btnClose.backgroundColor = UIColor.clear
        btnClose.lblTitle.isHidden = true
        btnClose.btnSubmit.setImage(UIImage.init(named: "ic-back-arrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnClose.btnSubmit.tintColor = UIColor.white
        btnClose.btnSubmit.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        btnClose.delegate = self
        btnClose.tag = 1
        self.view.addSubview(btnClose)
        
        logoImage.image = UIImage.init(named: "tufleet-logo")
        logoImage.clipsToBounds = true
        logoImage.contentMode = .scaleToFill
        self.view.addSubview(logoImage)
        
        passwordOldTF.placeHolderText = "Old Password"
        passwordOldTF.type = .password(toggleable: true)
        passwordOldTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(passwordOldTF)
        
        passwordNewTF.placeHolderText = "New Password"
        passwordNewTF.type = .password(toggleable: true)
        passwordNewTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(passwordNewTF)
        
        btn = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btn.backgroundColor = UIColor.init(hexString: "#63a95a")
        btn.layer.cornerRadius = 25
        btn.lblTitle.text = "CHANGE PASSWORD"
        btn.delegate = self
        btn.tag = 2
        self.view.addSubview(btn)
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        self.view.addSubview(activityIndicator!)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        btnClose.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(20)
            make.leading.equalTo(self.view.snp.leading).offset(0)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        
        bgImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        logoImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(100)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(63)
        }
        
        passwordOldTF.snp.makeConstraints { (make) in
            make.top.equalTo(logoImage.snp.bottom).offset(40)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        passwordNewTF.snp.makeConstraints { (make) in
            make.top.equalTo(passwordOldTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        btn.snp.makeConstraints { (make) in
            make.top.equalTo(passwordNewTF.snp.bottom).offset(40)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
    }
    
    @objc func pressed(sender: UIButton!) {
        
    }
    
    func changePassAction() {
        activityIndicator?.startAnimating()
        btn.btnSubmit.isEnabled = false
        
        let param = ["oldpassword":passwordOldTF.text!,"newpassword":passwordNewTF.text!]
        
        UserController().postEditPassword(param: param, onSuccess: { (code, message, results) in
            print(message)
            print("Do action when data success to fetching here")
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: "Sukses", message: results, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (alert) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }

}

extension ProfileViewController: ButtonAppDelegate {
    func submit(view: ButtonView) {
        if view.tag == 2 {
            changePassAction()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
