//
//  NewOrderViewController.swift
//  Tufleet
//
//  Created by Agoerie on 9/25/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import Photos
import SnapKit
import SwiftyJSON
import GooglePlaces
import CoreLocation
import MaterialDesignSymbol

class NewOrderViewController: UIViewController {
    
    var placesClient: GMSPlacesClient!
    
    var scroll = UIScrollView()
    var contentView = UIView()
    var contentOneView = InputContent()
    var valDestView = SummaryDestination()
    var contentTwoView = InputContent()
    var contentThirdView = OptionContent()
    var contentFiveView = OptionContent()
    
    var stackVw = UIStackView()
    var contentDateView = InputIcoContent()
    var contentTimeView = InputIcoContent()
    
    var titleOne = UILabel()
    var titleTwo = UILabel()
    var titleThird = UILabel()
    
    var attachment = UIImageView()
    var titleAttach = UILabel()
    
    var btn = ButtonView()
    
    var activityIndicator: UIActivityIndicatorView?
    
    var arrVal: [[String: String]] = []
    var paramDestVal: [[String: String]] = []
    var paramVal: [String: Any] = [:]
    
    var listDest = [DestinationModel]() {
        didSet {
            dataDest = listDest
        }
    }
    
    var dataDest :[DestinationModel] = []
    
    var place: GMSPlace?
    
    var isCurrent: Bool = true
    var isWihtClient: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        placesClient = GMSPlacesClient.shared()
        
        setupCurrentLoc()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupCurrentLoc() {
        placesClient.currentPlace { (placeLikelihoodList, error) in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                self.setupView()
                
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    print(place.name)
                    self.place = place
                }
            }
            
            self.setupView()
        }
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.white
        
        scroll.backgroundColor = UIColor.init(hexString: "#eaeefe")
        self.view.addSubview(scroll)
        
        contentView.backgroundColor = UIColor.clear
        scroll.addSubview(contentView)
        
        contentOneView = Bundle.main.loadNibNamed("InputContent", owner: nil, options: nil)?.first as! InputContent
        contentOneView.delegate = self
        contentOneView.isLoc = true
        contentOneView.data = [self.place?.name != nil ? (self.place?.name)! : "Asal","Destination :"]
        contentOneView.backgroundColor = UIColor.white
        contentView.addSubview(contentOneView)
        
        valDestView = Bundle.main.loadNibNamed("SummaryDestination", owner: nil, options: nil)?.first as! SummaryDestination
        valDestView.backgroundColor = UIColor.white
        valDestView.data = dataDest
        valDestView.delegate = self
        valDestView.isDisableBtn = false
        valDestView.updatesConstraint(margin: 10)
        contentView.addSubview(valDestView)
        
        contentTwoView = Bundle.main.loadNibNamed("InputContent", owner: nil, options: nil)?.first as! InputContent
        contentTwoView.data = ["Remark :"]
        contentTwoView.backgroundColor = UIColor.white
        contentView.addSubview(contentTwoView)
        
        contentThirdView = Bundle.main.loadNibNamed("OptionContent", owner: nil, options: nil)?.first as! OptionContent
        contentThirdView.data = ["One Way (Pickup or Drop Only)","Two Ways"]
        contentThirdView.backgroundColor = UIColor.white
        contentView.addSubview(contentThirdView)
        
        stackVw.axis = .horizontal
        stackVw.distribution = .fillEqually
        stackVw.alignment = .fill
        stackVw.spacing = 8
        contentView.addSubview(stackVw)
        
        contentDateView = Bundle.main.loadNibNamed("InputIcoContent", owner: nil, options: nil)?.first as! InputIcoContent
        contentDateView.isDate = true
        contentDateView.inputTF.tag = 1
        contentDateView.inputTF.placeholder = "Date"
        contentDateView.layer.cornerRadius = 5
        contentDateView.clipsToBounds = true
        contentDateView.icoLbl.text = MaterialDesignIcon.viewAgenda24px
        stackVw.addArrangedSubview(contentDateView)
        
        contentTimeView = Bundle.main.loadNibNamed("InputIcoContent", owner: nil, options: nil)?.first as! InputIcoContent
        contentTimeView.isDate = true
        contentTimeView.inputTF.tag = 2
        contentTimeView.inputTF.placeholder = "Time"
        contentTimeView.layer.cornerRadius = 5
        contentTimeView.clipsToBounds = true
        contentTimeView.icoLbl.text = MaterialDesignIcon.accessTime24px
        stackVw.addArrangedSubview(contentTimeView)
        
        contentFiveView = Bundle.main.loadNibNamed("OptionContent", owner: nil, options: nil)?.first as! OptionContent
        contentFiveView.data = ["Regular","VIP","With Client"]
        contentFiveView.backgroundColor = UIColor.white
        contentView.addSubview(contentFiveView)
        
        titleOne.text = "TRAVEL DETAIL"
        titleOne.font = UIFont.init(name: "AvenirNext-DemiBold", size: 18)
        titleOne.textColor = UIColor.darkGray
        contentView.addSubview(titleOne)
        
        titleTwo.text = "TRIP"
        titleTwo.font = UIFont.init(name: "AvenirNext-DemiBold", size: 18)
        titleTwo.textColor = UIColor.darkGray
        contentView.addSubview(titleTwo)
        
        titleThird.text = "TRANSPORTATION TYPE"
        titleThird.font = UIFont.init(name: "AvenirNext-DemiBold", size: 18)
        titleThird.textColor = UIColor.darkGray
        contentView.addSubview(titleThird)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapAttachment(_:)))
        attachment.image = UIImage.init(named: "add-placeholder")
        attachment.restorationIdentifier = "add-placeholder"
        attachment.contentMode = .scaleAspectFill
        attachment.clipsToBounds = true
        attachment.isUserInteractionEnabled = true
        attachment.addGestureRecognizer(tapGesture)
        contentView.addSubview(attachment)
        
        titleAttach.text = "Add Attachment"
        titleAttach.font = UIFont.init(name: "AvenirNext-DemiBold", size: 11)
        titleAttach.textColor = UIColor.darkGray
        titleAttach.isHidden = true
        contentView.addSubview(titleAttach)
        
        btn = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btn.backgroundColor = UIColor.init(hexString: "#63a95a")
        btn.layer.cornerRadius = 25
        btn.lblTitle.text = "ORDER"
        btn.delegate = self
        contentView.addSubview(btn)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        scroll.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(scroll)
            make.left.right.equalTo(view)
        }
        
        titleOne.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(contentView).offset(20)
        }
        
        contentOneView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(titleOne.snp.bottom).offset(10)
        }
        
        valDestView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(contentOneView.snp.bottom).offset(-15)
        }
        
        contentTwoView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(valDestView.snp.bottom).offset(20)
        }
        
        titleTwo.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(contentTwoView.snp.bottom).offset(30)
        }
        
        contentThirdView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(titleTwo.snp.bottom).offset(10)
        }
        
        stackVw.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(contentThirdView.snp.bottom).offset(20)
        }
        
        titleThird.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(stackVw.snp.bottom).offset(30)
        }
        
        contentFiveView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(20)
            make.top.equalTo(titleThird.snp.bottom).offset(10)
        }
        
        attachment.snp.makeConstraints { (make) in
            make.top.equalTo(contentFiveView.snp.bottom).offset(10)
            make.left.equalTo(titleThird)
            make.width.equalTo(60)
            make.height.equalTo(0)
        }
        
        titleAttach.snp.makeConstraints { (make) in
            make.bottom.equalTo(attachment)
            make.left.equalTo(attachment.snp.right).offset(10)
        }
        
        btn.snp.makeConstraints { (make) in
            make.left.right.equalTo(contentView).inset(40)
            make.top.equalTo(attachment.snp.bottom).offset(30)
            make.bottom.equalTo(contentView).offset(-30)
            make.height.equalTo(50)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getIsReview), name: .UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideAttachment(_:)), name: .transporType, object: nil)
        
        getIsReview()
        setDataDest()
    }
    
    @objc func getIsReview() {
        OrderController().checkReview(onSuccess: { (code, message, result) in
        }, onFailed: { (message) in
            self.btn.btnSubmit.isEnabled = false
            self.btn.backgroundColor = UIColor.init(hexString: "#63a95a").withAlphaComponent(0.5)
        }) { (message) in
            self.btn.btnSubmit.isEnabled = true
            self.btn.backgroundColor = UIColor.init(hexString: "#63a95a").withAlphaComponent(1)
        }
    }
    
    func setDataDest() {
        var destinations = [DestinationModel]()
        for item in JSON(arrVal).arrayValue {
            let destination = DestinationModel(json: item)
            destinations.append(destination)
        }
        
        listDest = destinations
        
        valDestView.data = dataDest
        valDestView.table.reloadData()
        
        if listDest.count == 0 {
            valDestView.updateTableConstraint(margin: 0)
            
            contentTwoView.snp.updateConstraints { (make) in
                make.top.equalTo(valDestView.snp.bottom).offset(30)
            }
        } else {
            contentTwoView.snp.updateConstraints { (make) in
                make.top.equalTo(valDestView.snp.bottom).offset(20)
            }
        }
        
        self.view.layoutIfNeeded()
    }
    
    func addNewDest(addr: String, lat: String, lng: String) {
        arrVal.append(["nama":addr])
        paramDestVal.append(["address":addr, "latitude": lat, "longitude": lng])
        
        setDataDest()
    }
    
    func deleteDest(row: Int) {
        arrVal.remove(at: row)
        paramDestVal.remove(at: row)
        
        setDataDest()
    }
    
    func openPlaceScrenn() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func hideAttachment(_ notification: Notification) {
        if let cell = notification.object as? OptionCell {
            if cell.optionLbl.text == "With Client" {
                attachment.snp.updateConstraints { (make) in
                    make.height.equalTo(60)
                }
                
                titleAttach.isHidden = false
                isWihtClient = true
            } else {
                attachment.snp.updateConstraints { (make) in
                    make.height.equalTo(0)
                }
                
                titleAttach.isHidden = true
                isWihtClient = false
            }
        }
    }
    
    @objc func tapAttachment(_ gesture: UIGestureRecognizer) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func submitAtachment(idBooking: String) {
        let data = UIImageJPEGRepresentation(attachment.image!, 1.0)
        
        activityIndicator?.startAnimating()
        btn.btnSubmit.isEnabled = false
        
        let params = ["id_booking":idBooking,"attachment":data] as [String: Any]
        
        OrderController().uploadImage(param: params, onSuccess: { (code, message, result) in
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: message, message: result, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (alert) in
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.toHomeScreen()
            }))
            self.present(alert, animated: true, completion: nil)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }

}

extension NewOrderViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place id: \(place.placeID)")
        print("Place lat: \(place.coordinate.latitude)")
        print("Place lng: \(place.coordinate.longitude)")
        dismiss(animated: true) {
            if self.isCurrent {
                let cell = self.contentOneView.tables.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! InputCell
                cell.inputTF.text = place.name
                
                self.contentOneView.selectTF = place.name
                self.place = place
            } else {
                self.addNewDest(addr: place.name + ", " + place.formattedAddress!, lat: "\(place.coordinate.latitude)", lng: "\(place.coordinate.longitude)")
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension NewOrderViewController: inputContentDelegate {
    func onTFDidUpdate(view: InputContent) {
        isCurrent = view.tags == 1 ? true : false
        
        openPlaceScrenn()
    }
}

extension NewOrderViewController: destDelegate {
    func onDelete(cell: SummaryDestination, row: Int) {
        deleteDest(row: row)
    }
}

extension NewOrderViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let photoAsset = info[UIImagePickerControllerPHAsset] as? PHAsset {
            if let filename = photoAsset.value(forKey: "filename") {
                attachment.restorationIdentifier = filename as! String
            }
        }
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        attachment.image = image
        dismiss(animated:true, completion: nil)
    }
}

extension NewOrderViewController: ButtonAppDelegate {
    func submit(view: ButtonView) {
        var lat = place?.coordinate.latitude ?? 0
        var lng = place?.coordinate.longitude ?? 0
        var dist: Double = 0
        
        for item in paramDestVal {
            guard let itemLat = item["latitude"] else { return }
            guard let itemLng = item["longitude"] else { return }

            let latTwo = Double(round(10000000*Double(itemLat)!)/10000000)
            let lngTwo = Double(round(10000000*Double(itemLng)!)/10000000)
            
            let locFirst = CLLocation(latitude: lat, longitude: lng)
            let locTwo = CLLocation(latitude: latTwo, longitude: lngTwo)
            let distNow = locFirst.distance(from: locTwo)
            
            lat = latTwo
            lng = lngTwo
            
            dist += distNow
        }
        
        let distanceInKilometer = dist /  1000
        
        paramVal = [
            "trip":contentThirdView.selectData,
            "tanggal_perjalanan":contentDateView.dateSelect,
            "jam_perjalanan":contentTimeView.dateSelect,
            "estimasi_jarak":String(format:"%.0f", ceil(distanceInKilometer)),
            "tipe":contentFiveView.selectData,
            "asal":(place?.name)!,
            "lat_asal":place?.coordinate.latitude ?? "",
            "lng_asal":place?.coordinate.longitude ?? "",
            "remark": contentTwoView.selectTF,
            "destinasi": paramDestVal
        ]
        
        activityIndicator?.startAnimating()
        btn.btnSubmit.isEnabled = false
        
        if isWihtClient {
            if self.attachment.restorationIdentifier == "add-placeholder" {
                self.activityIndicator?.stopAnimating()
                self.btn.btnSubmit.isEnabled = true
                
                let alert = UIAlertController.init(title: "Error", message: "Please attach image", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
        }
        
        OrderController().postOrder(param: paramVal, onSuccess: { (code, message, results) in
            print(message)
            
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            if self.isWihtClient {
                self.submitAtachment(idBooking: results![1])
            } else {
                let alert = UIAlertController.init(title: message, message: results![0], preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (alert) in
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.toHomeScreen()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }
}

