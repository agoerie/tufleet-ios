//
//  SummaryDetailViewController.swift
//  Tufleet
//
//  Created by Agoerie on 10/16/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit

class SummaryDetailViewController: UIViewController {
    
    var navbar = NavBarBack()
    var scroll = UIScrollView()
    var contentView = UIView()
    var content = SummaryDetailContent()
    var ratingContent = RatingContent()
    
    var data: OrderModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        let review = (data?.review)! as NSString
        
        self.view.backgroundColor = UIColor.init(hexString: "#ebebeb")
        
        navbar = Bundle.main.loadNibNamed("NavBarBack", owner: nil, options: nil)?.first as! NavBarBack
        navbar.titleLbl.text = "Order Summary"
        navbar.delegate = self
        self.view.addSubview(navbar)
        
        scroll.backgroundColor = UIColor.clear
        self.view.addSubview(scroll)
        
        contentView.backgroundColor = UIColor.clear
        self.scroll.addSubview(contentView)
        
        content = Bundle.main.loadNibNamed("SummaryDetailContent", owner: nil, options: nil)?.first as! SummaryDetailContent
        content.backgroundColor = UIColor.clear
        content.data = data
        content.setupView()
        self.contentView.addSubview(content)
        
        ratingContent = Bundle.main.loadNibNamed("RatingContent", owner: nil, options: nil)?.first as! RatingContent
        ratingContent.backgroundColor = UIColor.clear
        ratingContent.ratingView.value = CGFloat(((data?.rating)! as NSString).floatValue)
        ratingContent.reviewLbl.text = "\u{201C}" + "\(review)" + "\u{201D}"
        ratingContent.reviewLbl.isHidden = data?.review == "" ? true : false
        ratingContent.ratingView.isHidden = data?.rating == "" ? true : false
        ratingContent.ratingView.isUserInteractionEnabled = false
        self.contentView.addSubview(ratingContent)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        navbar.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(0)
            make.leading.equalTo(self.view.snp.leading).offset(0)
            make.trailing.equalTo(self.view.snp.trailing).offset(0)
            make.height.equalTo(84)
        }
        
        scroll.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(navbar.snp.bottom).offset(0)
            make.bottom.equalTo(view.snp.bottom).offset(0)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(scroll)
            make.left.right.equalTo(view)
        }
        
        content.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView.snp.top).offset(0)
            make.leading.equalTo(self.contentView.snp.leading).offset(0)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(0)
        }
        
        ratingContent.snp.makeConstraints { (make) in
            make.top.equalTo(content.snp.bottom).offset(0)
            make.leading.equalTo(self.contentView.snp.leading).offset(0)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(0)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(0)
        }
    }

}

extension SummaryDetailViewController: NavbarBackDelegate {
    func back(navbar: UIView) {
        self.navigationController?.popViewController(animated: true)
    }
}
