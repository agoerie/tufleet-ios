//
//  OrderViewController.swift
//  Tufleet
//
//  Created by Agoerie on 10/25/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import Tabman
import Pageboy
import SnapKit

class OrderViewController: TabmanViewController, PageboyViewControllerDataSource {
    
    private var viewControllers = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        bar.location = .top
        bar.style = .buttonBar
        
        bar.appearance = TabmanBar.Appearance({ (appearance) in
            appearance.state.color = UIColor.gray
            appearance.state.selectedColor = UIColor.init(hexString: "#fc4582")
            appearance.text.font = .systemFont(ofSize: 16.0)
            appearance.indicator.color = UIColor.init(hexString: "#fc4582")
            appearance.style.background = .solid(color: UIColor.init(hexString: "#f7f9f9"))
        })
        
        self.bar.items = [Item(title: "ORDER"),
                          Item(title: "SUMMARY")]
        self.dataSource = self
    }
    
    private func initializeViewControllers() {
        var viewControllers = [UIViewController]()
        
        let orderNew = NewOrderViewController()
        viewControllers.append(orderNew)
        
        let orderSummary = SummaryListViewController()
        viewControllers.append(orderSummary)
        
        self.viewControllers = viewControllers
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        initializeViewControllers()
        return 2
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return self.viewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }

}
