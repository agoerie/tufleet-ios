//
//  SummaryListViewController.swift
//  Tufleet
//
//  Created by Agoerie on 9/29/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit

class SummaryListViewController: UIViewController {
    
    var table = UITableView()
    
    var listOrder = [OrderModel]() {
        didSet {
            temps = listOrder
            table.reloadData()
        }
    }
    
    var temps :[OrderModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.init(hexString: "#ebebeb")
        
        let xib = SummaryListCell.nib
        table.register(xib, forCellReuseIdentifier: SummaryListCell.identifier)
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.tableFooterView = UIView()
        table.backgroundColor = UIColor.clear
        self.view.addSubview(table)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        table.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(0)
            make.leading.equalTo(self.view.snp.leading).offset(0)
            make.trailing.equalTo(self.view.snp.trailing).offset(0)
            make.bottom.equalTo(self.view.snp.bottom).offset(0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getDataOrder), name: .UIApplicationDidBecomeActive, object: nil)
        
        getDataOrder()
    }
    
    @objc func getDataOrder() {
        OrderController().getListOrder(onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            self.listOrder = res
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }

}

extension SummaryListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = listOrder[indexPath.row]
        
        return SummaryListCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: object)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = SummaryDetailViewController()
        vc.data = listOrder[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}

extension SummaryListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
