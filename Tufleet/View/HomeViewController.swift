//
//  HomeViewController.swift
//  Tufleet
//
//  Created by Agoerie on 10/25/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyJSON

class HomeViewController: UIViewController {
    
    var navbar = NavBarLogo()
    var order = OrderViewController()
    
    var activityIndicator: UIActivityIndicatorView?
    
    var listOrder = [OrderModel]() {
        didSet {
            temps = listOrder
        }
    }
    
    var temps :[OrderModel] = []
    
    lazy var name: String = {
        if let userData = UserDefaults.standard.string(forKey: "userdata") {
            let jsonArray = JSON.init(parseJSON: userData).arrayValue
            return jsonArray[2].stringValue
        }
        
        return ""
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.white
        
        navbar = Bundle.main.loadNibNamed("NavBarLogo", owner: nil, options: nil)?.first as! NavBarLogo
        navbar.nameLbl.text = name
        navbar.delegate = self
        self.view.addSubview(navbar)
        
        addChildViewController(order)
        self.view.addSubview(order.view)
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        self.view.addSubview(activityIndicator!)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        navbar.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view.snp.leading).offset(0)
            make.trailing.equalTo(self.view.snp.trailing).offset(0)
            make.top.equalTo(self.view.snp.top).offset(0)
            make.height.equalTo(64)
        }
        
        order.view.snp.makeConstraints { (make) in
            make.top.equalTo(navbar.snp.bottom).offset(0)
            make.leading.equalTo(self.view.snp.leading).offset(0)
            make.trailing.equalTo(self.view.snp.trailing).offset(0)
            make.bottom.equalTo(self.view.snp.bottom).inset(0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getDataOrder), name: .UIApplicationDidBecomeActive, object: nil)
        
        getDataOrder()
        
    }
    
    @objc func getDataOrder() {
        OrderController().getListOrder(onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            self.listOrder = res
            
            self.checkLastOrder(data: res)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }
    
    func checkLastOrder(data: [OrderModel]) {
        let filterArray = data.filter { $0.status == "Done" }.filter { $0.rating == "" }
        
        if filterArray.count > 0 {
            // to rating screen
            toRatingPage(data: filterArray[0])
            
            return
        }
    }
    
    func toRatingPage(data: OrderModel) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.rating) as! RatingViewController
        vc.dataOrder = data
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func postRating(data: OrderModel, rating: Int, comment: String) {
        self.activityIndicator?.startAnimating()
        
        let param = ["id_booking":data.idBooking,"rating":rating,"review":comment] as [String : Any]
        
        OrderController().postReview(param: param, onSuccess: { (code, message, result) in
            self.activityIndicator?.stopAnimating()
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            
            self.activityIndicator?.stopAnimating()
            
            self.getDataOrder()
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.toHomeScreen()
        }
    }
    
    func logout() {
        if UserDefaults().contains(key: "userdata") {
            UserDefaults.standard.removeObject(forKey: "userdata")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.toLoginScreen()
        }
    }
    
    func toProfile() {
        let vc = ProfileViewController()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension HomeViewController: NavbarLogoDelegate {
    func menu(navbar: UIView) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Menu.bar) as! MenuBarViewController
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
}

extension HomeViewController: ratingDelegate {
    func pressSubmit(controller: RatingViewController, data: OrderModel, rating: Int, comment: String) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        postRating(data: data, rating: rating, comment: comment)
    }
}

extension HomeViewController: menuViewDelegate {
    
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenu(controller: MenuBarViewController, menu: String) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        switch menu {
        case "Profile":
            toProfile()
        default:
            logout()
        }
    }
    
}
