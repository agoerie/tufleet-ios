//
//  RegisterViewController.swift
//  Tufleet
//
//  Created by Agoerie on 9/22/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import AnimatedTextInput

class RegisterViewController: UIViewController {
    
    var btnClose = ButtonView()
    var bgImage = UIImageView()
    var logoImage = UIImageView()
    var usernameTF = AnimatedTextInput()
    var fullnameTF = AnimatedTextInput()
    var emailTF = AnimatedTextInput()
    var phoneTF = AnimatedTextInput()
    var groupTF = AnimatedTextInput()
    var floorTF = AnimatedTextInput()
    var btn = ButtonView()
    var activityIndicator: UIActivityIndicatorView?
    
    var idGroup: Int?
    var idFloor: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        bgImage.image = UIImage.init(named: "bgtugu.jpg")
        bgImage.clipsToBounds = true
        bgImage.contentMode = .scaleToFill
        self.view.addSubview(bgImage)
        
        btnClose = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btnClose.backgroundColor = UIColor.clear
        btnClose.lblTitle.isHidden = true
        btnClose.btnSubmit.setImage(UIImage.init(named: "ic-back-arrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnClose.btnSubmit.tintColor = UIColor.white
        btnClose.btnSubmit.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        btnClose.delegate = self
        btnClose.tag = 1
        self.view.addSubview(btnClose)
        
        logoImage.image = UIImage.init(named: "tufleet-logo")
        logoImage.clipsToBounds = true
        logoImage.contentMode = .scaleToFill
        self.view.addSubview(logoImage)
        
        usernameTF.placeHolderText = "Username"
        usernameTF.type = .standard
        usernameTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(usernameTF)
        
        fullnameTF.placeHolderText = "Full Name"
        fullnameTF.type = .standard
        fullnameTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(fullnameTF)
        
        emailTF.placeHolderText = "Email"
        emailTF.type = .email
        emailTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(emailTF)
        
        phoneTF.placeHolderText = "Phone Number"
        phoneTF.type = .phone
        phoneTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(phoneTF)
        
        groupTF.placeHolderText = "Group"
        groupTF.type = .standard
        groupTF.delegate = self
        groupTF.tag = 1
        groupTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(groupTF)
        
        floorTF.placeHolderText = "Lantai"
        floorTF.type = .standard
        floorTF.delegate = self
        floorTF.tag = 2
        floorTF.style = CustomTextInputRegisterStyle() as AnimatedTextInputStyle
        self.view.addSubview(floorTF)
        
        btn = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btn.backgroundColor = UIColor.init(hexString: "#63a95a")
        btn.layer.cornerRadius = 25
        btn.lblTitle.text = "REGISTER"
        btn.delegate = self
        btn.tag = 2
        self.view.addSubview(btn)
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        self.view.addSubview(activityIndicator!)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        btnClose.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(20)
            make.leading.equalTo(self.view.snp.leading).offset(0)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        
        bgImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        logoImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(100)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(63)
        }
        
        usernameTF.snp.makeConstraints { (make) in
            make.top.equalTo(logoImage.snp.bottom).offset(40)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        fullnameTF.snp.makeConstraints { (make) in
            make.top.equalTo(usernameTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        emailTF.snp.makeConstraints { (make) in
            make.top.equalTo(fullnameTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        phoneTF.snp.makeConstraints { (make) in
            make.top.equalTo(emailTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        groupTF.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        floorTF.snp.makeConstraints { (make) in
            make.top.equalTo(groupTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        btn.snp.makeConstraints { (make) in
            make.top.equalTo(floorTF.snp.bottom).offset(40)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
    }
    
    @objc func pressed(sender: UIButton!) {
        
    }
    
    func selectGroup() {
        let vc = GroupViewController()
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func selectFloor() {
        let vc = FloorViewController()
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func registerAction() {
        if idGroup == nil {
            let alert = UIAlertController.init(title: "Warning", message: "Please fill group field.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        activityIndicator?.startAnimating()
        btn.btnSubmit.isEnabled = false
        
        let param = ["username":usernameTF.text!,"fullname":fullnameTF.text!,"email":emailTF.text!,"group":"\(idGroup!)","lantai":"\(idFloor)","no_hp":phoneTF.text!]
        
        UserController().postRegister(param: param, onSuccess: { (code, message, results) in
            print(message)
            print("Do action when data success to fetching here")
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: "Sukses", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (alert) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }

}

extension RegisterViewController: AnimatedTextInputDelegate {
    func animatedTextInputShouldBeginEditing(animatedTextInput: AnimatedTextInput) -> Bool {
        if animatedTextInput.tag == 1 {
            selectGroup()
        }
        
        if animatedTextInput.tag == 2 {
            selectFloor()
        }
        
        return false
    }
}

extension RegisterViewController: ButtonAppDelegate {
    func submit(view: ButtonView) {
        print("Submit Btn Biasa")
        if view.tag == 2 {
            registerAction()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension RegisterViewController: groupViewDelegate {
    func onDismissGroup(controller: GroupViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenuGroup(controller: GroupViewController, menu: String, id: Int) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        groupTF.text = menu
        idGroup = id
    }
}

extension RegisterViewController: floorViewDelegate {
    func onDismiss(controller: FloorViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenu(controller: FloorViewController, menu: String, id: Int) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        floorTF.text = menu
        idFloor = id
    }
}

struct CustomTextInputRegisterStyle: AnimatedTextInputStyle {
    let placeholderInactiveColor = UIColor.white
    let activeColor = UIColor.white
    let inactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineInactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineActiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineHeight: CGFloat = 1
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 17)
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 9
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 15
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 2
    let yHintPositionOffset: CGFloat = 0
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}
