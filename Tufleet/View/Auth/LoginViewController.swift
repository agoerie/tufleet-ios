
//
//  LoginViewController.swift
//  Tufleet
//
//  Created by Agoerie on 9/21/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyJSON
import AnimatedTextInput
import MaterialDesignSymbol

class LoginViewController: UIViewController {
    
    var bgImage = UIImageView()
    var logoImage = UIImageView()
    var registerButton = UIButton()
    var usernameTF = AnimatedTextInput()
    var iconUserLabel = UILabel()
    var passwordTF = AnimatedTextInput()
    var iconPassLabel = UILabel()
    var btn = ButtonView()
    var btnForgot = ButtonView()
    var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setupView() {
        bgImage.image = UIImage.init(named: "bgtugu.jpg")
        bgImage.clipsToBounds = true
        bgImage.contentMode = .scaleToFill
        self.view.addSubview(bgImage)
        
        logoImage.image = UIImage.init(named: "tufleet-logo")
        logoImage.clipsToBounds = true
        logoImage.contentMode = .scaleToFill
        self.view.addSubview(logoImage)
        
        registerButton.backgroundColor = UIColor.init(hexString: "#fc4582")
        registerButton.setTitle("Registrasi", for: .normal)
        registerButton.setTitleColor(UIColor.white, for: .normal)
        registerButton.titleLabel?.font = UIFont.init(name: "AvenirNext-DemiBold", size: 18)
        registerButton.addTarget(self, action: #selector(pressed(sender:)), for: .touchUpInside)
        registerButton.showsTouchWhenHighlighted = true
        self.view.addSubview(registerButton)
        
        usernameTF.placeHolderText = "Username"
        usernameTF.type = .standard
        usernameTF.style = CustomTextInputLoginStyle() as AnimatedTextInputStyle
        self.view.addSubview(usernameTF)
        
        iconUserLabel.font = MaterialDesignFont.fontOfSize(18)
        iconUserLabel.text = MaterialDesignIcon.person24px
        iconUserLabel.textColor = UIColor.white
        self.view.addSubview(iconUserLabel)
        
        passwordTF.placeHolderText = "Password"
        passwordTF.type = .password(toggleable: true)
        passwordTF.style = CustomTextInputLoginStyle() as AnimatedTextInputStyle
        self.view.addSubview(passwordTF)
        
        iconPassLabel.font = MaterialDesignFont.fontOfSize(18)
        iconPassLabel.text = MaterialDesignIcon.lock24px
        iconPassLabel.textColor = UIColor.white
        self.view.addSubview(iconPassLabel)
        
        btn = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btn.backgroundColor = UIColor.init(hexString: "#63a95a")
        btn.layer.cornerRadius = 25
        btn.lblTitle.text = "LOGIN"
        btn.tag = 1
        btn.delegate = self
        self.view.addSubview(btn)
        
        btnForgot = Bundle.main.loadNibNamed("ButtonView", owner: nil, options: nil)?.first as! ButtonView
        btnForgot.backgroundColor = UIColor.clear
        btnForgot.lblTitle.text = "Forgot Password ?"
        btnForgot.tag = 2
        btnForgot.delegate = self
        self.view.addSubview(btnForgot)
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        self.view.addSubview(activityIndicator!)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        bgImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        logoImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(100)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(63)
        }
        
        registerButton.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
            make.height.equalTo(55)
        }
        
        usernameTF.snp.makeConstraints { (make) in
            make.top.equalTo(logoImage.snp.bottom).offset(40)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        iconUserLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(usernameTF.snp.centerY).offset(5)
            make.leading.equalTo(usernameTF.snp.leading).offset(0)
            make.width.equalTo(19)
            make.height.equalTo(19)
        }
        
        passwordTF.snp.makeConstraints { (make) in
            make.top.equalTo(usernameTF.snp.bottom).offset(15)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).offset(-25)
        }
        
        iconPassLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordTF.snp.centerY).offset(5)
            make.leading.equalTo(passwordTF.snp.leading).offset(0)
            make.width.equalTo(19)
            make.height.equalTo(19)
        }
        
        btn.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTF.snp.bottom).offset(40)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(45)
        }
        
        btnForgot.snp.makeConstraints { (make) in
            make.top.equalTo(btn.snp.bottom).offset(0)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
    }
    
    @objc func pressed(sender: UIButton!) {
        toRegisterView()
    }
    
    func loginAction() {
        activityIndicator?.startAnimating()
        btn.btnSubmit.isEnabled = false
        
        UserController().postLogin(param: ["username":usernameTF.text!,"password":passwordTF.text!], onSuccess: { (code, message, results) in
            guard let data = results else { return }
            
            let jsonString = JSON(data).rawString()
            UserDefaults.standard.set(jsonString, forKey: "userdata")
            
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.btn.btnSubmit.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            self.toHomeScreen()
        }
    }
    
    func toHomeScreen() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.toHomeScreen()
    }
    
    func toForgotPass() {
        let vc = ForgotPassViewController()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func toRegisterView() {
        let vc = RegisterViewController()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension LoginViewController: ButtonAppDelegate {
    func submit(view: ButtonView) {
        print("Submit Btn Biasa \(view.tag)")
        if view.tag == 1 {
            // to login action
            loginAction()
        } else {
            // to forgot pass screen
            toForgotPass()
        }
    }
}

struct CustomTextInputLoginStyle: AnimatedTextInputStyle {
    let placeholderInactiveColor = UIColor.white
    let activeColor = UIColor.white
    let inactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineInactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineActiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineHeight: CGFloat = 1
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 17)
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 9
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 20
    let topMargin: CGFloat = 15
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 2
    let yHintPositionOffset: CGFloat = 0
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}
