//
//  MenuBarViewController.swift
//  Tufleet
//
//  Created by Agoerie on 10/25/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit

protocol menuViewDelegate: class {
    func onDismiss(controller: MenuBarViewController)
    func onSelectMenu(controller: MenuBarViewController,menu: String)
}

class MenuBarViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = MenuBarCell.nib
            table.register(xib, forCellReuseIdentifier: MenuBarCell.identifier)
            
            table.delegate = self
            table.dataSource = self
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
            
            table.isScrollEnabled = false
        }
    }
    
    open weak var delegate: menuViewDelegate?
    
    let data = ["Profile","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        delegate?.onDismiss(controller: self)
    }
    
}

extension MenuBarViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return MenuBarCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.onSelectMenu(controller: self, menu: data[indexPath.row])
    }
}

extension MenuBarViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
}
