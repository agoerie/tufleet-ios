//
//  RatingViewController.swift
//  Tufleet
//
//  Created by Agoerie on 10/25/18.
//  Copyright © 2018 YKDigital. All rights reserved.
//

import UIKit
import HCSStarRatingView
import AnimatedTextInput

protocol ratingDelegate: class {
    func pressSubmit(controller: RatingViewController, data: OrderModel, rating: Int, comment: String)
}

class RatingViewController: UIViewController {
    
    @IBOutlet var lblCourir: UILabel!
    @IBOutlet var viewRating: HCSStarRatingView!
    @IBOutlet var txtComment: AnimatedTextInput!
    @IBOutlet var btnSubmit: UIButton!
    
    open weak var delegate: ratingDelegate?
    
    var dataOrder: OrderModel?
    var rating: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        btnSubmit.layer.cornerRadius = 22.5
        
        viewRating.value = 0
        viewRating.starBorderColor = UIColor.lightGray
        viewRating.tintColor = UIColor.init(hexString: "FFFF00")
        
        txtComment.placeHolderText = "Write Comment"
        txtComment.type = .multiline
        txtComment.style = RatingTextInputStyle() as AnimatedTextInputStyle
        txtComment.showCharacterCounterLabel(with: 85)
        txtComment.delegate = self
        
        lblCourir.text = dataOrder?.driver
    }
    
    struct RatingTextInputStyle: AnimatedTextInputStyle {
        let placeholderInactiveColor = UIColor.white
        let activeColor = UIColor.gray.withAlphaComponent(0.5)
        let inactiveColor = UIColor.gray.withAlphaComponent(0.5)
        let lineInactiveColor = UIColor.gray.withAlphaComponent(0.2)
        let lineActiveColor = UIColor.white.withAlphaComponent(1.0)
        let lineHeight: CGFloat = 1
        let errorColor = UIColor.red
        let textInputFont = UIFont.systemFont(ofSize: 13)
        let textInputFontColor = UIColor.gray
        let placeholderMinFontSize: CGFloat = 9
        let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
        let leftMargin: CGFloat = 0
        let topMargin: CGFloat = 20
        let rightMargin: CGFloat = 0
        let bottomMargin: CGFloat = 0
        let yHintPositionOffset: CGFloat = 7
        let yPlaceholderPositionOffset: CGFloat = 0
        public let textAttributes: [String: Any]? = nil
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if rating == 0 {
            let alert = UIAlertController.init(title: nil, message: "Berikan rating terlebih dahulu.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        if txtComment.text == "" {
            let alert = UIAlertController.init(title: nil, message: "Berikan review terlebih dahulu.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        delegate?.pressSubmit(controller: self, data: dataOrder!, rating: rating, comment: txtComment.text!)
    }
    
    @IBAction func ratingChange(_ sender: HCSStarRatingView) {
        rating = Int(sender.value)
    }

}

extension RatingViewController: AnimatedTextInputDelegate {
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        return false
    }
    
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let text = animatedTextInput.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        
        return newLength <= 85
    }
}
