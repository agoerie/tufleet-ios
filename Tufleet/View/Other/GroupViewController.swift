//
//  GroupViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/3/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SnapKit

protocol groupViewDelegate: class {
    func onDismissGroup(controller: GroupViewController)
    func onSelectMenuGroup(controller: GroupViewController,menu: String,id: Int)
}

class GroupViewController: UIViewController {
    
    var table = UITableView()
    
    open weak var delegate: groupViewDelegate?
    
    var listGroup = [ListGroup]() {
        didSet {
            temps = listGroup
            table.reloadData()
        }
    }
    
    var temps :[ListGroup] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        getDataGroup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.init(hexString: "#222222").withAlphaComponent(0.8)
        
        let xib = GroupCell.nib
        table.register(xib, forCellReuseIdentifier: GroupCell.identifier)
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.tableFooterView = UIView()
        table.layer.cornerRadius = 5
        self.view.addSubview(table)
        
        setupConstraint()
    }
    
    func setupConstraint() {
        let height = CGFloat(listGroup.count * 45)
        
        table.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.view.snp.centerY)
            make.leading.equalTo(self.view.snp.leading).offset(25)
            make.trailing.equalTo(self.view.snp.trailing).inset(25)
            make.height.equalTo(height)
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        delegate?.onDismissGroup(controller: self)
    }
    
    func getDataGroup() {
        MenuController().getListGroup(onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            self.listGroup = res
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            let heights = self.listGroup.count * 45
            self.table.snp.updateConstraints({ (make) in
                make.height.equalTo(CGFloat(heights > 422 ? 422 : heights))
            })
        }
    }

}

extension GroupViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listGroup[indexPath.row]
        
        return GroupCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = listGroup[indexPath.row]
        
        delegate?.onSelectMenuGroup(controller: self, menu: data.groupName, id: data.id)
    }
}

extension GroupViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}
